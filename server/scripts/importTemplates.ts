import XLSX from 'xlsx';
import _ from 'lodash';
import parse from 'csv-parse/lib/sync';
import prettier from 'prettier';
import { organizations } from '../../common/organizations';
import {
  ElectionData,
  OrganizationEndorsements,
  RaceMetadata,
  Section,
} from '../../common/types';

function camelcaseKeys(obj: Record<string, any>): any {
  return _.zipObject(
    Object.keys(obj).map(k => _.camelCase(k)),
    Object.values(obj),
  );
}

interface RaceMetadataWithNameKey extends RaceMetadata {
  nameKey: string;
  section: string;
}

interface SectionWithoutMetadata extends Omit<Section, 'metadata'> {}

async function run() {
  const file = process.argv[2];
  const workbook = XLSX.readFile(file);

  // Import races
  const racesSheet = workbook.Sheets.Races;
  if (!racesSheet) throw new Error('Could not find Races sheet');

  const races: RaceMetadataWithNameKey[] = (XLSX.utils.sheet_to_json(
    racesSheet,
  ) as Record<string, string>[]).map(item => camelcaseKeys(item));

  // Import sections
  const sectionsSheet = workbook.Sheets.Sections;
  if (!sectionsSheet) throw new Error('Could not find Sections sheet');

  const sectionsWithoutMetadata: SectionWithoutMetadata[] = (XLSX.utils.sheet_to_json(
    sectionsSheet,
  ) as Record<string, string>[]).map(item => camelcaseKeys(item));

  const organizationEndorsements: {
    [orgName: string]: OrganizationEndorsements;
  } = {};

  for (const sheetName of workbook.SheetNames) {
    const matchingOrg = organizations.find(org =>
      org.name.startsWith(sheetName),
    );
    if (!matchingOrg) {
      console.warn('SKIPPED: Could not find', sheetName);
      continue;
    }

    const orgSheet = workbook.Sheets[sheetName];
    const parsed = parse(XLSX.utils.sheet_to_csv(orgSheet));

    const orgName = matchingOrg.name;

    const endorsements: { [raceId: string]: string | string[] } = {};
    for (const row of parsed) {
      const [raceName, ...allValues] = row;
      const race = races.find(r => r.nameKey === raceName);
      const nonEmptyValues = (allValues as string[]).filter(v => !!v);

      if (race && nonEmptyValues.length > 0) {
        // We have an endorsement
        endorsements[race.race] =
          nonEmptyValues.length > 1 ? nonEmptyValues : nonEmptyValues[0];
      }
    }

    const link = parsed[0][1];
    organizationEndorsements[orgName] = {
      endorsements,
      link,
    };
  }

  // Generate the code: we need 2 things:
  // 1. Section[]
  // 2. OrganizationEndorsements[]
  const racesBySection = _.groupBy(races, r => r.section);
  const sections = sectionsWithoutMetadata.map(section => ({
    ...section,
    metadata: racesBySection[section.heading].map(
      ({ nameKey, section, ...race }): RaceMetadata => race,
    ),
  }));

  const data: ElectionData = {
    sections,
    endorsements: organizationEndorsements,
  };

  const code = `
import { ElectionData } from '../../common/types';

const electionData: ElectionData = ${JSON.stringify(data, null, 2)};
export default electionData;
`;
  console.log(
    prettier.format(code, {
      parser: 'babel',
      singleQuote: true,
      trailingComma: 'all',
    }),
  );
}

run();
